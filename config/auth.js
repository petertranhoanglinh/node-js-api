
const auth = async (req,res,next) => {
    try {
        const token = req.header('Authorization').replace('Bearer', '').trim()
        const tokenCookie  = req.cookies['jwt']
        if(token != tokenCookie){
            res.status(401).send({error:'Please authenticate!'})
        }
        next()
    } catch (error) {
        res.status(401).send({error:'Please authenticate!'})
    }
}

module.exports = auth