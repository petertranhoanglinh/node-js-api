const WebSocket = require('ws');
const Binance = require('node-binance-api');

function createWebSocketServer(server) {
  const wss = new WebSocket.Server({ server });
  const binance = new Binance().options({
    'family': 4,
  });

  wss.on('connection', (ws) => {
    console.log('WebSocket client connected');

    ws.on('message', (message) => {
      console.log('received: %s', message);
      ws.send(`Server received: ${message}`);
    });

    ws.on('close', () => {
      console.log('WebSocket client disconnected');
    });

    // Gửi dữ liệu từ Binance tới client qua WebSocket khi có sự kiện mini ticker
    // setInterval(() => {
    //   binance.futuresMiniTickerStream((data) => {
    //     // Gửi dữ liệu về client
    //     ws.send(JSON.stringify(data));
    //   });
    // }, 1 * 60 * 1000); //
  });
}

module.exports = createWebSocketServer;
