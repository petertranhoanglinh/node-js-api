const User = require('../modes/userModel')
const multer = require('multer')
const getUserDetail = async (_id) =>{
    const user = await User.findOne({_id : _id})
    const {password , ...data} = await user.toJSON(); // khong lay password ra
    return data;
}

const storage = (_urlPath) => multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null,_urlPath); // Thư mục lưu trữ file
    },
    filename: (req, file, cb) => {
      cb(null,String( file.originalname + '_' + Date.now())); // Đặt tên file
    },
});

const skipPagination =async (page, limit)  =>{
    page = parseInt(page) || 1;
    limit = parseInt(limit) || 10;
    return  (page * limit) - limit;
}

module.exports = {
    getUserDetail,
    storage,
    skipPagination
}

