
const mongoose = require('mongoose')
const connectDB = require('../db'); 
const bcrypt = require('bcrypt');
const User = require("../modes/userModel");
const jwt = require("jsonwebtoken");

const SECRET_KEY_JWT = process.env.SECRET_KEY_JWT;
connectDB();
const { getUserDetail } = require('../common/common');
exports.addUser = async (req,res)=>{

    try {
        const password  = req.body.password;
        // Hash the password before storing it
        const hashedPassword = await bcrypt.hash(password, 10);
        const userNew = new User(req.body);
        userNew.password = hashedPassword;
        let result = await userNew.save();
        res.json({'pass': result});
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
    
}

exports.auth = async (req,res) =>{
    try {
        const email = req.body.email;
        result = await User.findOne( {email : email})
        if (!result || !(await bcrypt.compare(req.body.password,result.password ))) {
            return res.status(401).send('Invalid credentials');
        }
        const token = jwt.sign({_id :result._id} , SECRET_KEY_JWT);
        let {password , ...data} = await result.toJSON();
        data = {...data , jwt : token}
        res.cookie('jwt' , token , {
            httpOnly:true,
            maxAge:24*60*60*1000 // 1hour
        })
        res.status(200).send(data);
        
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

exports.userDetail = async (req,res) =>{
    try {
        cookies = req.cookies['jwt']
        const claims = jwt.verify(cookies , SECRET_KEY_JWT)
        const data = await  getUserDetail(claims._id);
        res.send(data);
    } catch (error) {
        res.status(500).json({msg :error.message})
    }
}

exports.logout = async (req,res) =>{
    try {
        res.cookie('jwt' , '' ,{maxAge:0} )
        // Additional response logic if needed
        res.json({msg:'logout suscess'})
    }
    catch (error) {
        res.status(500).json({msg :error.message})
    }

}