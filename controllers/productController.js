const Product = require("../modes/productModel")
const mongoose = require('mongoose')
const connectDB = require('../db'); 

connectDB();
var numberRequest = 0;


exports.getProduct = async (req,res)=>{
    try {
       const productName = req.params.productName;
       let product
       if(productName == '*'){
         product = await Product.find();
       }else{
         product = await Product.find({ name: { $regex: productName, $options: 'i' } });
       }
       res.json(product);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

exports.getProductById = async (req ,res) =>{
    try {
        const product = await Product.findOne({_id : req.params._id})
        res.json(product)
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}



exports.addProduct = async (req , res) =>{
    try 
    {
    const path = 'uploads/product'
    let result = {};
    let id = req.body._id;
    if(req.files){
        req.body.large =  path +'/'+  req.files.large[0].filename ;
        req.body.cart  =  path +'/'+ req.files.cart[0].filename   ;
        req.body.promotion =  path +'/'+ req.files.promotion[0].filename ;
        req.body.detail    =  path +'/'+ req.files.detail[0].filename ;
    }
    if (id != undefined || id != null) {
        result = await Product.findByIdAndUpdate(
        id
        ,res.body
        , { new: true } 
        )
        res.json({ msg:'update success', code: 'OK' });
    } else {
        const product = new Product(req.body);
        result = await product.save();
        res.json({ msg:'insert success', code: 'OK' });
    }
    } catch (error) {
          res.status(500).json({ msg: error.message, code: 'NOT_OK' });
    }
}
exports.test = async(req , res) =>{
    try {
        ++numberRequest;
        if(numberRequest > 5){
            return   res.status(500).json({ message: "server is busy" });
        }
        const consumer = mongoose.connection.collection('customer')
        const documents = await consumer.find({}).toArray();
        console.log(req.requestTime);
        res.status(200).json(documents);
    } catch (error) {   
        res.status(500).json({ message: error.message });
    }
}

exports.myIp = async(req,res, next) =>{
    try {
        const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        req.ipAddress = ipAddress;
        res.status(200).json({myIp : ipAddress});
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

exports.addTest = async(req,res) =>{
    try {
        const data = req.body;
        const consumer = database.collection('customer');
        // Insert the record into the collection
        const result = await consumer.insertOne(data);
        res.status(200).json({ message: 'save OK'});
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}