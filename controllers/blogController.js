const Blog = require("../modes/blogModel")
const BlogImage = require("../modes/blogImageModel")
const mongoose = require('mongoose')
const connectDB = require('../db'); 
const {skipPagination} = require('../common/common')
connectDB();

exports.addBlog = async (req , res) =>{
    try 
    {
        let id = req.body._id;
        let result;
        if (id != undefined || id != null) {
            result = await Blog.findByIdAndUpdate(
            id
            ,res.body
            , { new: true } 
            )
            res.json({ msg:'update success', code: 'OK' });
        } else{
            const blog = new Blog(req.body);
            result = await blog.save();
            res.json({ msg:'insert success', code: 'OK' , value: result._id});
        }

        } catch (error) {
            res.status(500).json({ msg: error.message, code: 'NOT_OK' });
        }
}

exports.addImgBlog = async (req , res) =>{
    try 
    {
        const path = 'uploads/blog';
        let result;
        if(req.file){
            req.body.image =  path +'/'+  req.file.filename ;
        }
        console.log(req.body.image)
        const blogImg = new BlogImage(req.body);
        result = await blogImg.save();
        res.json({ msg:'insert success', code: 'OK' });
        } catch (error) {
            res.status(500).json({ msg: error.message, code: 'NOT_OK' });
        }
}

exports.getBlogs = async(req,res) =>{
    let id = req.body._id;
   
    const skip = await skipPagination(req.body.page , req.body.limit)
    try {
    let blog;
    if(id != undefined || id != null){
        const objectId = new mongoose.Types.ObjectId(id);
        blog = await Blog.aggregate([
            { $match: { _id:objectId } },
            { $lookup: { from: 'blogimages', localField: '_id', foreignField: 'blogId', as: 'images' } },
            { $skip: skip },
            { $limit: req.body.limit },
          ]);
    }else{
        blog = await Blog.aggregate([
            {
              $lookup: {
                from: 'blogimages',
                localField: '_id',
                foreignField: 'blogId',
                as: 'images',
              },
            },
            {
              $skip: skip, // Bỏ qua số lượng kết quả cần bỏ qua
            },
            {
              $limit: req.body.limit, // Giới hạn số lượng kết quả trả về
            },
          ]);
    }

    
      res.json(blog);
    } catch (error) {
      res.status(500).json({ msg: error.message , code:'NOT_OK'});
    }
}   
   
