const Account = require("../modes/accountModel")
const mongoose = require('mongoose')
const connectDB = require('../db');
connectDB();
function generateRandomString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
exports.addAccount = async (req, res) => {
    try {
        const { username, password, apiKey, apiSecretKey } = req.body;
        // Kiểm tra xem các trường bắt buộc đã được cung cấp chưa
        if (!username || !password || !apiKey || !apiSecretKey) {
            return res.status(400).json({ message: "Please provide username, password, apiKey, and apiSecretKey" });
        }
        // Kiểm tra xem tài khoản đã tồn tại trong cơ sở dữ liệu chưa
        let existingAccount = await Account.findOne({ username });
        if (existingAccount) {
            // Nếu tài khoản đã tồn tại, cập nhật thông tin của nó
            existingAccount.password = password;
            existingAccount.apiKey = apiKey;
            existingAccount.apiSecretKey = apiSecretKey;
            // Lưu cập nhật vào cơ sở dữ liệu
            await existingAccount.save();
            const result = await Account.findOne({ _id: existingAccount._id }).select('username key');
            return res.status(200).json({ message: "Account updated successfully", data: result });
        } else {
            // Nếu tài khoản chưa tồn tại, tạo mới và lưu vào cơ sở dữ liệu
            const key = generateRandomString(10); // Độ dài của chuỗi ngẫu nhiên là 10 ký tự
            const newAccount = new Account({
                username,
                password,
                apiKey,
                apiSecretKey,
                key
            });
            // Lưu bản ghi vào cơ sở dữ liệu
            await newAccount.save();
            const result = await Account.findOne({ _id: newAccount._id }).select('username key');
            return res.status(201).json({ message: "Account created successfully", data: result });
        }
    } catch (error) {
        console.error("Error creating/updating account:", error);
        return res.status(500).json({ message: "Internal server error" });
    }
}

exports.getAccountByKey = async (req, res) => {
    try {
        const key = req.body.key;
        // Tìm tài khoản bằng key
        const account = await Account.findByKey(key);
        if (!account) {
            return res.status(404).json({ message: "Account not found" });
        }
        // Trả về thông tin tài khoản (apiKey và apiSecretKey)
        const binance = res.locals.binance
        binance.balance((error, balances) => {
            if (error) {
                return res.status(404).json({ message: 'Could not connect to Binance' });
            } else {
                return res.status(200).json({ message: "balance", data:  balances});
            }
        });
       
    } catch (error) {
        console.error("Error retrieving account:", error);
        return res.status(500).json({ message: "Internal server error" });
    }
}

exports.testConnect = async (req , res) => {
    try {
        
    } catch (error) {
        
    }
}





