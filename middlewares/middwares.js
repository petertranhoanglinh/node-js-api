const Account = require("../modes/accountModel");
const mongoose = require('mongoose');
const connectDB = require('../db');
const Binance = require('node-binance-api');

connectDB();

async function connectBinance(req, res, next) {
    const key = req.body.key;
    try {
        const account = await Account.findByKey(key);
        if (!account) {
            return res.status(404).json({ message: "Account not found" });
        }
        
        // Khởi tạo đối tượng Binance với options
        const binance = new Binance().options({
            APIKEY: account.apiKey,
            APISECRET: account.apiSecretKey,
            family: 4 
        });

        // Gọi phương thức balance() để kiểm tra kết nối

        binance.balance((error, balances) => {
            if (error) {
                return res.status(404).json({ message: 'Could not connect to Binance' });
            } else {
                res.locals.binance = binance;
                next();
            }
        });
    } catch (error) {
        console.error("Could not connect to Binance:", error);
        return res.status(500).json({ message: error });
    }
}

module.exports = connectBinance;
