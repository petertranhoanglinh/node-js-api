const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const http = require('http');
const WebSocket = require('ws');
dotenv.config({ path: 'process.env' });
const productRoutes = require('./routers/productRouter');
const userRouter = require('./routers/userRouter');
const blogRouter = require('./routers/blogRouter');
const accountRouter = require('./routers/accountRouter');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser');
const path = require('path');
const expressLayouts = require('express-ejs-layouts');
const ejs = require('ejs');

const app = express();
app.use(expressLayouts);

app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'ejs');

app.use(cors({
  credentials:true,
  origin:['http://localhost:3000' ,'http://localhost:4200' , 'https://travel-blog-aaaaa.web.app']
}));
app.use(cookieParser());
app.use(bodyParser.json());

const requestTime =  function (req , res , next){
  req.requestTime = Date.now();
  next();
};
app.use(requestTime);
const port = 3000;
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/asset', express.static(path.join(__dirname, 'asset')));

app.use('/api/product', productRoutes);
app.use('/api/user', userRouter);
app.use('/api/blog', blogRouter);
app.use('/api/account', accountRouter);

app.get('/', (req, res) => {
  res.render('index',  { extractScripts: true }); 
});

app.get('/gioi-thieu', (req, res) => {
  res.render('intro' ,  { extractScripts: true }); 
});

app.get('/tai-khoan', (req, res) => {
  res.render('account' ,  { extractScripts: true }); 
});

app.get('/client', (req, res) => {
  res.render('client' ,  { extractScripts: true }); 
});

const createWebSocketServer = require('./config/websocketServer');
const server = http.createServer(app);
createWebSocketServer(server);

server.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});