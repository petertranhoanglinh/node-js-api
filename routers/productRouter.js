const express = require('express');
const multer = require('multer')
const router = express.Router();
router.use(express.json());
const productController = require('../controllers/productController');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads/product'); // Thư mục lưu trữ file
    },
    filename: (req, file, cb) => {
      cb(null,file.originalname +'_'+String(Date.now())); // Đặt tên file
    },
});
  
const upload = multer({ storage: storage });

router.get('/getTest', productController.test);
router.post('/addTest', productController.addTest);
router.post('/addProduct', upload.fields([{ name: 'large' }, { name: 'cart' } 
, { name: 'detail' }, { name: 'promotion' }]) ,  productController.addProduct);
router.get('/getProduct/:productName', productController.getProduct);
router.get('/getProductById/:_id', productController.getProductById);
router.get('/myIp', productController.myIp);
module.exports = router;
