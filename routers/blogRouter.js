const express = require('express');
const multer = require('multer')
const router = express.Router();
router.use(express.json());

const blogController = require('../controllers/blogController')

const { storage} = require('../common/common')
const upload = multer({ storage: storage('uploads/blog') });

router.post('/add', blogController.addBlog);
router.post('/image/add', upload.single('file'), blogController.addImgBlog);

router.post('/gets', blogController.getBlogs);
module.exports = router;
