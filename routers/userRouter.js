const express = require('express');
const auth = require('../config/auth')
const router = express.Router();
router.use(express.json());
const userController = require('../controllers/userController')
router.post('/addUser', userController.addUser);
router.post('/auth', userController.auth);
router.get('/userDetail', auth ,userController.userDetail);
router.post('/logout', userController.logout);
module.exports = router;
