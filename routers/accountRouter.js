const express = require('express');
const connectBinnace = require('../middlewares/middwares')
//const auth = require('../config/auth')
const router = express.Router();
router.use(connectBinnace);
const accountController = require('../controllers/accountController')
router.post('/addAccount', accountController.addAccount);
router.get('/getKey' ,accountController.getAccountByKey);
module.exports = router;
