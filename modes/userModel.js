const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
    {
        username:{
            type:String,
            require:[true,"Please enter the username"]
        },

        email:{
            type:String,
            unique: true,
            require:[true,"Please enter the email"]
        },
        role:{
            type:String,
            require:[true],
            default:'U00'
        },
        password:{
            type:String,
            require:[true],
        }

    },
    {
        timestamps:true
    }

)

const User = mongoose.model("User",userSchema );
module.exports = User;