const mongoose = require('mongoose');

const blogSchema = mongoose.Schema(
       {
        title:{
            type:String,
            require:[true,"Please enter a product title"]
        },
        fristContent:{
            type:String,
            require:[true,"Please enter a fristContent"]
        },
        bodyContent:{
            type:String,
            require:[true,"Please enter a bodyContent"]
        },
        endContent:{
            type:String,
            require:[true,"Please enter a endContent"]
        }
    },
    {
        timestamps:true
    }

)

const Blog = mongoose.model("Blog",blogSchema );
module.exports = Blog;