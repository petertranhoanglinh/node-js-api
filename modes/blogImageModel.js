const mongoose = require('mongoose');

const blogImageSchema = mongoose.Schema(
    {
        blogId:{
            type:mongoose.Schema.Types.ObjectId,
            ref: 'Blog',
        },
        image:{
            type:String,
            require:[true,""]
        },
        kind:{
            type:String,
            require:[true, ""] // frist , body , end ,  any
        }
       
    },
    {
        timestamps:true
    }

)

const BlogImage = mongoose.model("BlogImage",blogImageSchema );
module.exports = BlogImage;