const mongoose = require('mongoose');

const productSchema = mongoose.Schema(
    {
        name:{
            type:String,
            require:[true,"Please enter a product name"]
        },

        quantity:{
            type:Number,
            require:[true],
            default:0
        },
        price:{
            type:Number,
            require:[true],

        },
        large:{
            type:String,
            require:false
        },
        detail:{
            type:String,
            require:false
        },
        cart:{
            type:String,
            require:false
        },
        promotion:{
            type:String,
            require:false
        },
        description:{
            type:String,
            require:false
        }

    },
    {
        timestamps:true
    }

)

const Product = mongoose.model("Product",productSchema );
module.exports = Product;