const mongoose = require('mongoose');

const accountSchema = mongoose.Schema(
    {
        username:{
            type:String,
            require:[true,"Please enter the username"]
        },

        password:{
            type:String,
            unique: true,
            require:[true,"Please enter the password"]
        },
        apiKey:{
            type:String,
            require:[true],
            require:[true,"Please enter the api key"]
        },
        apiSecretKey:{
            type:String,
            require:[true],
            require:[true,"Please enter the api secrect key"]
        },
        key:{
            type:String,
            require:[false]
        }

    },
    {
        timestamps:true
    }

)

// Function để tìm tài khoản dựa trên trường key
accountSchema.statics.findByKey = async function(key) {
    const account = await this.findOne({ key });
    return account;
};


const Account = mongoose.model("Account",accountSchema );
module.exports = Account;