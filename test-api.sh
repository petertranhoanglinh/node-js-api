#!/bin/bash

# Số lần lặp
n=1000

# Thời gian giữa các yêu cầu (trong giây)
sleep_duration=0.001  # 1 giây / 1000

# Vòng lặp để gửi yêu cầu 1000 lần
for ((i=1; i<=$n; i++)); do
    # Gửi yêu cầu bằng cURL
    
# curl -X POST "https://csewing.hansoll.com/Sewing/SewingOutputMonitor2_getData" \
#      -H "Cookie: ASP.NET_SessionId=kwdxckpdhmsr3l1dwp5loz45; __RequestVerificationToken=mVEmKPe3piW2rKVOO5T_TlZph01VIDhuJy2kqPQvJ7pOyatHWZVt1LWmaZGyieoSBUPdyxYQwhqVbUO7_JNWL28ArlT_JUsfnG5hLzq6Mn41; Language=; .AspNet.ApplicationCookie=6mAUbzpxTP7vB4n2vOpw8gRq0zV2d14x5eJ7Ok4Lerxcg_zM-M87OMVQcu9r8FBIq7zdNt0x83Cz7xKb09cZXyEZg7G9yDOAs8YGXUqsv77aO0VHfuivIx8YOZ1mhAX8Qhdnxc63agbSs5OKl9CM2s0u049bZB6asm8nM37pW8-yIaa9EjSPjp8ZABXFkS7fy5ThT5jJ27Zk4z1hxXODLP7UhjmAw3IXAm2yadt_txaHBJkz_iFobmEbdBJTmW6zxC32oVCIvFJeilmHOPJfciBqpb5Ae6wbeU3b3k0Jsz93q1n59OHEzt5EJt_A2XiRd8jEJcEJuRiAYdCh3PiGBY4BOAzP_HfKDXqiLS3PlXOm0mPNkc--O6LoLnb_hM7llyUKzRy2NOa7QedS2pi4ImHjyXgjfQco2qW78rt3SuA5tJWeqzhlRnawVTTINNN02ANu5j2jJakGrB-vuDCXjZyrkdm1JQDy1WpxzjCEowvlQcajBsMmODIXeb3IQ9nm" \
#      -H "Content-Type: application/json" \
#      -d '{"FACTORY_ID":"V031","DATE":"2024-03-14","LINE_ID":"","DIV_LINE":""}' >/dev/null &

    # Đợi một khoảng thời gian trước khi gửi yêu cầu tiếp theo
    sleep $sleep_duration
done





